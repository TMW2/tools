#! /usr/bin/env python3
# -*- coding: utf8 -*-
#
# Copyright (C) 2018~2020  TMW-2
# Author: Jesusalva
# Note: This will "force" completion to 100%. Use review system!

import copy, datetime
import polib, yaml

defaultLang = "en"
rootPath = "../../site/i18n/"

langs=[]
files={}
originals={}
tm={defaultLang: ["","\n"]}

# Search for array[?]==search in an array of dicts
# Returns the dictionary, or returns "ERROR"
def dl_search(array, search):
    for key in array.keys():
        if array[key] == search:
            return key
    raise Exception("Key not found")

# For headers
def currentdate():
    dt=datetime.datetime.now()

    day=dt.timetuple()[2]
    month=dt.timetuple()[1]
    year=dt.timetuple()[0]

    hour=dt.timetuple()[3]
    minute=dt.timetuple()[4]
    #second=5
    #weekday=6
    #yearday=7

    return "%04d-%02d-%02d %02d:%02d-0300" % (year, month, day, hour, minute)


def init():
    global defaultLang, rootPath, langs, files, originals, tm
    # Populate langs
    o=open("langs.txt", "r")
    for i in o:
        langs.append(i.replace('\n',''))
    o.close()

    # Create the original file
    f=open(rootPath+defaultLang+".yml", "r")
    originals=yaml.load(f, Loader=yaml.FullLoader) # Dictionary
    f.close()

    # We don't care with metadata
    del originals["language_meta"]

    # Create each language's template
    for i in langs:
        files[str(i)]=copy.copy(originals)
        tm[str(i)]=["","\n"]

    """
    # Do the reading for each language
    for i in langs:
        f=open(rootpath+i+".yml", "r")
        files[str(i)]=yaml.load(f, Loader=yaml.FullLoader) # Dictionary
        f.close()
    """

# [OK] Returns name from language code
def lgname(lg):
    if lg == "en":
        return "English"
    elif lg == "pt-BR":
        return "Português (Brasil)"
    elif lg == "de":
        return "Deutsch"
    elif lg == "fr":
        return "François"
    elif lg == "es":
        return "Español"
    else:
        return "Unknown %s" % lg

# [OK] Entry
def poentry(org, ttl, comments):
    return polib.POEntry(msgid=org, msgstr=ttl, comment=comments)

# Creates/Loads stuff
def generatePoFiles():
    global tm
    context=copy.copy(langs)
    context.append('en')
    for lg in context:
        print("Updating po file for "+lg)
        po=polib.POFile()
        po.metadata = {
            'Project-Id-Version': '1.0',
            'Report-Msgid-Bugs-To': 'dev@tmw2.org',
            'POT-Creation-Date': currentdate(),
            'PO-Revision-Date': currentdate(),
            'Last-Translator': 'TMW2 Team <dev@tmw2.org>',
            'MIME-Version': '1.0',
            'Content-Type': 'text/plain; charset=utf-8',
            'Content-Transfer-Encoding': '8bit',
        }

        for key, speech in originals.items():
            # Translation Memory (no duplicates)
            if speech in tm[lg]:
                continue
            tm[lg].append(speech)

            # Add to po file
            if lg == "en":
                po.append(poentry(speech, "", key))
            else:
                po.append(poentry(speech, files[lg][key], key))

        po.save("po/%s.po" % lg)
    context.remove('en')





# [OK] Reads Po Files
def readPoFile(lg):
    try:
        po=polib.pofile("po/"+lg+".po")
    except:
        a=open("po/"+lg+".po", "w")
        a.close()
        print("%s.po does not exist, not reading" % lg)
        return

    print("%s Progress: %d%%" % (lgname(lg), po.percent_translated()))

    for entry in po:
        #print("%s = %s" % (entry.msgid, entry.msgstr))

        try:
            if entry.msgstr != "":
                dest=dl_search(files[lg], entry.msgid)
                #print("[+] %s" % dest)
                files[lg][dest]=entry.msgstr
            else:
                dest=dl_search(files[lg], entry.msgid)
                #print("%s.%s IS EMPTY" % (lg, dest))
                try:
                    files[lg][dest]=originals[dest]
                except:
                    print("%s - failed to obtain originals" % entry.msgid)
        except:
                print("%s - string was removed" % entry.msgstr)

# [OK] Save the new YML file
def writeLocal(lg):
    f=open(rootPath+lg+".yml", 'w')
    f.write("# THIS FILE WAS GENERATED AUTOMATICALLY\n#EDITING IT WILL HAVE NO EFFECT\n\n")
    files[lg]["language_meta"]={"code": lg, "name": lgname(lg)}
    yaml.dump(files[lg], f)
    f.close()











# Mainframe: setup
init()

# Save translations to their YML files
for lg in langs:
    readPoFile(lg)
    writeLocal(lg)

# Mainframe: handle PO files
generatePoFiles()

