#!/bin/bash

echo "Begin - Gameinfo updater. User input required."
cd ../../gameinfo-api

cd serverdata
git pull --rebase -X theirs
cd ..

cd clientdata
git pull --rebase -X theirs
cd ..

git commit -a
git push

echo "Gameinfo updated!"

