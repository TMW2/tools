#!/usr/bin/python3
# This converts lang files in po files for Duke M

import polib, traceback
from copy import copy

dc={}
s=True
k=""

f=open("lang_de.txt", "r")

for l in f:
    x=l.replace("\n", "")
    if not s:
        s=True
        continue
    elif k == "":
        k=copy(x)
        continue
    else:
        dc[k]=copy(x)
        s=False; k=""
        continue

f.close()
print("Total strings loaded: %d" % len(dc))

po=polib.pofile('in/de.po')
cnt=0
print("Current progress: "+str(po.percent_translated())+"%")
for e in po:
    try:
        if (dc[e.msgid] != ""):
            cnt+=1
            e.msgstr=copy(dc[e.msgid])
    except:
        traceback.print_exc()
        print("Faulty: %s" % e.msgid)

po.save()
print("Manipulated %d strings." % cnt)
print("Progress afterwards:"+str(po.percent_translated())+"%")



