// TMW2 Script
// Author:
//  Jesusalva
// Description:
//  This is the “Stranger” black box.
//  It contains the answers to all the riddles.
//  This file is kept outside the main tree for translation purposes.
//  You thought you could cheat the answers, eh?

//  Syntax:
//  0171_Stranger_BlackBox( difficulty )
//  Where difficulty can be EASY, MEDIUM or HARD.
//  Returns 1 on success, 0 on failure.
function	script	0171_Stranger_BlackBox	{
    switch (getarg(0)) {
        case EASY:
            setarray .@q$, l("Are you cheating?"), l("Should we blame Saulc for bugs?");
            setarray .@r$, "no", "yes";
            setarray .@l$, l("no"), l("yes");
            break;
        case MEDIUM:
            setarray .@q$, l("Are you cheating?"), l("Should we blame Saulc for bugs?");
            setarray .@r$, "no", "yes";
            setarray .@l$, l("no"), l("yes");
            break;
        case HARD:
            setarray .@q$, l("Are you cheating?"), l("Should we blame Saulc for bugs?");
            setarray .@r$, "no", "yes";
            setarray .@l$, l("no"), l("yes");
            break;
        default: return 0;
    }

    .@e=rand(getarraysize(.@q$)-1);
    mesq .@q$[.@e];
    input .@ans$;

    return riddlevalidation(.@ans$, .@r$[.@e], .@l$[.@e]);
}

